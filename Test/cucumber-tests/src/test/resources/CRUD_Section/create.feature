Feature: Creating a new Section
  It is essential to create a new section or subsection to use the application

  Scenario: Creating subsection from a parent section with valid name
    Given user is logged in
    And parent section is chosen
    And user clicked on create button
    And create-Section dialog is shown on screen
    And user added section name [name]
    And [name] is valid
    When user clicked on create
    Then section should be created
    And create-Section dialog should be closed
    And new section is visible

  Scenario: Creating subsection from a parent section with invalid name
    Given user is logged in
    And parent section is chosen
    And user clicked on create button
    And create-Section dialog is shown on screen
    When user added section name [name]
    And [name] is invalid
    Then create button should be disabled
    And cancel button should not be disabled
    And name-field should show error


  Scenario: Cancelling the creation of a subsection from a parent section
    Given user is logged in
    And parent section is chosen
    And user clicked on create button
    And create-Section dialog is shown on screen
    And user added section name [name]
    And [name] is valid
    When user clicked on cancel
    Then section should not be created
    And create-Section dialog should be closed



