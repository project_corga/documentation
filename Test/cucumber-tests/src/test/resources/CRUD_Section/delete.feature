Feature: Deleting a Section
  It is essential to be able to delete a section or subsection to keep a clean structure

  Scenario: Deleting subsection from a parent section
    Given user is logged in
    And to be deleted section is chosen
    And user clicked on delete button
    And delete-Section dialog is shown on screen
    When user clicked on delete
    Then subsection should be deleted
    And content of subsection should be deleted
    And delete dialog should be closed



  Scenario: Cancelling deletion of a subsection
    Given user is logged in
    And to be deleted section is chosen
    And user clicked on delete button
    And delete-Section dialog is shown on screen
    When user clicked on cancel
    Then subsection should not be deleted
    And delete dialog should be closed




