package de.corga

import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.junit.platform.engine.Cucumber
import io.github.bonigarcia.wdm.WebDriverManager
import org.assertj.core.api.Assertions.assertThat
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver


@Cucumber
internal class RunCucumberTest {
	private lateinit var driver: WebDriver
	
	@Before
	fun setup() {
		WebDriverManager.chromedriver().setup()
		driver = ChromeDriver()
	}
	
	@Given("today is Sunday")
	fun today_is_sunday() {
		println("today is sunday")
	}
	
	@Given("browser is open")
	fun browser_is_open() {
		driver.get("https://google.com")
		assertThat(driver.title).isEqualTo("Google")
	}
	
	@When("I ask whether it's Friday yet")
	fun i_ask_whether_it_s_Friday_yet() {
		println("i_ask_whether_it_s_Friday_yet")
	}
	
	@Then("I should be told {string}")
	fun i_should_be_told(expectedAnswer: String) {
		println("i_should_be_told: $expectedAnswer")
	}
	
	@After
	fun close() {
		driver.close()
	}
}
