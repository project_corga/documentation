plugins {
	kotlin("jvm") version "1.4.10"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
	mavenCentral()
}

dependencies {
	implementation(kotlin("stdlib"))
	
	testImplementation("org.seleniumhq.selenium:selenium-java:latest.release")
	testImplementation("org.junit.jupiter:junit-jupiter-api:latest.release")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:latest.release")
	testImplementation("io.cucumber:cucumber-java:latest.release")
	testImplementation("io.cucumber:cucumber-junit-platform-engine:latest.release")
	testImplementation("io.cucumber:cucumber-picocontainer:latest.release") //dependency injection
	testImplementation("io.github.bonigarcia:webdrivermanager:4.2.2") //web drivers
	testImplementation("org.assertj:assertj-core:3.11.1")
	
	//testImplementation("org.slf4j:slf4j-api:1.7.30")
	//testImplementation("org.slf4j:slf4j-log4j12:1.7.30")
}

tasks {
	test {
		systemProperties(System.getProperties().toMap() as Map<String, Any>)
		systemProperty("cucumber.execution.parallel.enabled", System.getProperty("test.parallel"))
		systemProperty("cucumber.filter.tags", "not @ignore")
		useJUnitPlatform {
			excludeTags("disabled")
		}
	}
}
