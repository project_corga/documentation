# CORGA - Software Requirements Specification 

## Table of contents
- [Table of contents](#table-of-contents)
- [Introduction](#1-introduction)
    - [Purpose](#11-purpose)
    - [Scope](#12-scope)
    - [Definitions, Acronyms and Abbreviations](#13-definitions-acronyms-and-abbreviations)
    - [References](#14-references)
    - [Overview](#15-overview)
- [Overall Description](#2-overall-description)
    - [Vision](#21-vision)
    - [Use Case Diagram](#22-use-case-diagram)
	- [Technology Stack](#23-technology-stack)
- [Specific Requirements](#3-specific-requirements)
    - [Functionality](#31-functionality)
    - [Usability](#32-usability)
    - [Reliability](#33-reliability)
    - [Performance](#34-performance)
    - [Supportability](#35-supportability)
    - [Design Constraints](#36-design-constraints)
    - [Online User Documentation and Help System Requirements](#37-on-line-user-documentation-and-help-system-requirements)
    - [Purchased Components](#purchased-components)
    - [Interfaces](#39-interfaces)
    - [Licensing Requirements](#310-licensing-requirements)
    - [Legal, Copyright And Other Notices](#311-legal-copyright-and-other-notices)
    - [Applicable Standards](#312-applicable-standards)
- [Supporting Information](#4-supporting-information)

## 1. Introduction

### 1.1 Purpose
This SRS's (Software Requirements Specification) purpose is documenting and describing all planned features and general specifications for the project CORGA. 
It starts with an overview and goes later on more into detail. This includes use cases, scope and requirements as listed in the table of contents.
 

### 1.2 Scope

This document will outline the development process of the project. CORGA allows users to put data like To Dos in Cards and order these cards in a hierarchy. The functionality will be explained in detail in the following points. 


### 1.3 Definitions, Acronyms and Abbreviations

| Definition | Explanation                            |
| -----------| -------------------------------------- |
| section    | a contentwise representation of an aspect of the users life. It can contain other subsections and cards. The user can switch between three different views. A breadcrumb helps navigating between sections.|
| subsection | a section expanding a superior section  |
| dashboard  | user specific section containing favourite cards/sections  |
| card      | contains data on two sides (front and back)             |
| calendarview         | a visualisation of the section specifically for appointment cards.               |
| listview         | a visualisation of the section. Cards and subsections are displayed as list items.               |
| bubbleview         | a visualisation of the section. Subsections are displayed as bubbles and cards visually.|


| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| SRS         | Software Requirements Specification    |
| UC          | Use Case                               |
| n/a         | not applicable                         |
| tbd         | to be determined                       |
| UCD         | overall Use Case Diagram               |


### 1.4 References
| Title                                                              | Date       | Publishing organization   |
| -------------------------------------------------------------------|:----------:| ------------------------- |
| [CORGA Blog](https://corga898830573.wordpress.com/)    | 21.10.2020 | CORGA Team    |
| [GitLab](https://gitlab.com/project_corga)              | 21.10.2020 | CORGA Team    |



### 1.5 Overview
Chapter 2 describes our vision and the UCD. In the third chapter specific requirements like functionality, usability, reliance and performance are going to be explained in greater detail. In the last chapter, additional supporting information is displayed.
    
## 2. Overall Description

### 2.1 Vision
The goal of this project is to realize an webbased organization platform with a unique concept for the representation of information. 

The idea of CORGA is to organize every aspect of the users life. This includes family, work, uni or additional projects. To keep a neat structure, aspects are divided into different sections.

These sections can be divided into subsections to form a structure that fits the users specific needs. To simplify the navigation between different sections CORGA provides a breadcrumb. Sharing these sections with other users will make it possible to work in teams. 

To keep a clean look, we want to provide a new way to visualize data in a consistent style. Every section contains several cards and additional sections. Information is displayed on a card with a front- and a backpage. Those can be customized according to the users preferences and needs. It will be possible to include pictures, documents, formulas, appointments, code-snippets and much more. To simplify the users creation process, CORGA provides a range of default layouts for the most common usages.
 
One special section is the userspecific dashboard. The dashboard should enable the user to get an overview about the most important cards by marking them as favourite. 
Finally, we want to make it possible to switch between three different views depending on the users preferences. 

This includes a calendarview, a classic listview and a bubbleview. The calendarview gives an overview over appointments of this or other sections. In the listview cards and sections are displayed as list items while the bubbleview provides a way for the user to read the content of his cards without having to open them first. 


### 2.2 Use Case Diagram
Here you can see our overall use case diagram with the implemented scope in yellow and red. It should be said that not everything in the OUCD has been implemented but possible extensions had been kept in mind while implementing the scope.
![OUCD]

### 2.3 Technology Stack
Project Management:
*   Model: SCRUM
*   Tools: YouTrack, Miro
*   Communication: Discord

Versioning:
*   Version Management: Git
*   Repository Manager: GitLab
*   Tools: SublimeMerge, GitBash

Frontend:
*   Framework: Angular
*   Programming-Languages:
    *   Typescript: heavily supported by Angular
    *   SCSS: preprocessing scripting language compiling in CSS
*   Buildtool: npm
*   Testframework: Jasmine, Cypress
*   IDE: Visual Studio Code

Backend:
*   Framework: Spring Boot
*   Programming-Languages: Kotlin
*   Buildtool: Gradle
*   Testframework: JUnit
*   IDE: IntelliJ
*   Database: MySQL


## 3. Specific Requirements

### 3.1 Functionality
This section explains the general scope and specifically the different use cases that can be seen in the UCD.

The project contains the following sub-systems: 

*   Section: contains cards and subsections. 
    * The idea is to divide aspects of the users life into sections like uni, work, family and friends. 
    * Every section has a list view of cards and subsections
    * Every view contains a breadcrumb that makes navigating between sections easier.
    * Sections can be marked as favourite. 

*   Card: contains information in text form like notes or even tables. 
    * The idea is to show the most important information on the front. By flipping the card, the user is able to read more details. 
    * Cards can be marked as favourite. 


*   Dashboard: the section containing information for a specific user. This includes the root sections and cards or sections he marked as favourite.

The scope until December contains the following UseCases:
*   [Manage Section](./UseCases/manage-section/ManageSection.md)
*   [Order In Hierarchy](./UseCases/order-in-hierarchy/OrderInHierarchy.md)
*   [Show Favourite](./UseCases/show-favourite/ShowFavourite.md)

The scope until June contains the following UseCases:
*   [Manage Card](./UseCases/manage-card/ManageCard.md)
*   [Mark as Favorite](./UseCases/mark-as-favourite/MarkAsFavourite.md)
*   [Customize Content](./UseCases/customize-content/CustomizeContent.md)

### 3.2 Usability
The user interface is intuitive and self-explanatory. In order to reach this goal, commonly used symbols (like heart for favourite) and tooltips will be used to explain the essential features of the program. 

### 3.3 Reliability
As this program is a web application, the user is not dependend on the abilities of his own personal computer. So the only thing to consider is to keep the server running to have the application accessible at all time. This is important so user can take a look at their notes whenever they feel like it.

### 3.4 Perfomance
As there is no user interaction but only communication between server and client the response time should be very low. Also, as the data that is stored in the database contains only text elements it should be possible to store and manage the data of at least one thousand users at the same time (for now).

### 3.5 Supportability
#### 3.5.1 Coding Standards
In order to make it as easy as possible to keep the project readable and extendable, we us the latest latest clean code standard as far as possible. 

#### 3.5.2 Testing Strategy
In order to create a good and thorough tested project, we use Unit Tests, Integration Tests and Systems Tests for Frontend and Backend and Acceptance Tests to make sure that potential users don't have any problems using the application.

### 3.6 Design Constraints
CORGA uses the Model View Controller Architecture to differntiate between User Interface and logic. This is explained in further detail in the Software Architecture Document. 

### 3.7 On-line User Documentation and Help System Requirements
As the application is very intuitive, no tutorials will be provided. However contact info is given in the impressum to make it possible to reach out in case this should be wanted.

### 3.8 Purchased Components
Apart from a server there are no purchased components.

### 3.9 Interfaces

#### 3.9.1 User Interfaces
The following user interfaces will be implemented: 
- **Dashboard** showing the root sections as well as favourite cards and sections in a list view
- **Section** showing Subsections and Cards created for this Section in a list view as well as Buttons to create new Cards or Subsections or mark existing ones as favourite
- **Card** showing all relevant data stored for this Card 
- **Breadcrumb and Header** make it possible to navigate between Sections and to the Dashboard.


#### 3.9.2 Hardware Interfaces
n/a

#### 3.9.3 Software Interfaces
CORGA runs on Chrome and Firefox on PCs. It is not optimized for mobile phones.

### 3.10 Licensing Requirements
The application can be used free of charge for any user who wants to do so provided it is not for commercial use.
Commercial users may approach the team to talk about a special made contract in further detail. 

### 3.11 Legal, Copyright, and Other Notices
CORGA team does not take responsibility for lost or incorrect data. The CORGA logo may only be used for the CORGA application.
### 3.12 Applicable Standards
The Coding standards of Clean Code had been used as far as possible.

## 4. Supporting Information
For any further information you can contact the CORGA Team or check our [CORGA Blog](https://corga898830573.wordpress.com/). 
The Team Members are:
- Jonas Bürgel
- Katja Heßmann
- Beatrice Wellmann

<!-- Picture-Link definitions: -->
[OUCD]: <./UseCases/overall-use-case-diagram.png> "Overall Use Case Diagramm"
