# CORGA-Team - Software Architecture Document

## Table of Contents

- [1. Introduction](#1-introduction)
  - [1.1 Purpose](#11-purpose)
  - [1.2 Scope](#12-scope)
  - [1.3 Definitions, Acronyms and Abbreviations](#13-definitions-acronyms-and-abbreviations)
  - [1.4 References](#14-references)
  - [1.5 Overview](#15-overview)
- [2. Architectural Representation](#2-architectural-representation)
  - [2.1 MVC](#21-MVC)
- [3. Architectural Goals and Constraints](#3-architectural-goals-and-constraints)
- [4. Use-Case View](#4-use-case-view)
  - [4.1 Use-Case Realizations](#41-use-case-realizations)
- [5. Logical View](#5-logical-view)
  - [5.1 Overview](#51-overview)
  - [5.2 Architecturally Significant Design Packages](#52-architecturally-significant-design-packages)
- [6. Process View](#6-process-view)
- [7. Deployment View](#7-deployment-view)
- [8. Implementation View](#8-implementation-view)
  - [8.1 Overview](#81-overview)
  - [8.2 Layers](#82-layers)
- [9. Data View](#9-data-view)
- [10. Size and Performance](#10-size-and-performance)
- [11. Quality](#11-quality)
- [12. Pattern](#12-pattern)

## 1. Introduction

### 1.1 Purpose

This document provides an architectural overview of the our system and should help to easily understand our software.

### 1.2 Scope

The scope of this SAD is to show the overall architecture of the CORGA project. Use-Cases and classes are depicted.

### 1.3 Definitions, Acronyms and Abbreviations

| Abbreviation |                                     |
| ------------ | ----------------------------------- |
| IDE          | Integrated Development Environment  |
| MVC          | Model View Controller               |
| n/a          | not applicable                      |
| SAD          | Software Architecture Document      |
| SRS          | Software Requirements Specification |
| tbd          | to be determined                    |
| UC           | Use Case                            |

| Definition                     |                                                                                                                                                                                              |
| ------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Software Architecture Document | The Software Architecture Document provides a comprehensive architectural overview of the system, using a number of different architectural views to depict different aspects of the system. |

### 1.4 References

| Title | link |
| ----- | ---- |
| SRS   |

### 1.5 Overview

This document contains the goals and constraints as well as the logical, deployment and implementation.

## 2. Architectural Representation

Our project uses the classic MVC structure for frontend and backend consisting of a model, a controller and a view which interact with each other as can be seen below.
![MVC]
https://www.techyourchance.com/wp-content/uploads/2015/06/MVC_MVP.png

## 3. Architectural Goals and Constraints

The goal for this project is to have a webapplication working that enables you to create sections and cards and share them with other users. Every user should have a dashboard to view the main sections as well as his favourite cards and sections.

Now for the MVC:
In the frontend we use interfaces to describe models. The controller is implemented through NgRx-statemanagement and angular services. The view is shown by angular components.

In the backend we use spring entities to describe the models and use repositories to access them. The controller is handled by spring services that communicate through data transfer objects with the view. For the view we use spring rest controller.

## 4. Use-Case View
### 4.1 Use-Case Realizations
Here you can see our Overall Use Case Diagram with the implemented functionalities (scope) marked in yellow and red.
The actual realization of these use cases is described in further detail in our Software Requirement Specification (SRS).

![OUCD]

## 5. Logical View

### 5.1 Overview

We used Spring for the backend which already has a predefined sturcture: Controller, Repository and Service. However this can be easily transformed into the MVC model as shown in the following pictures:
The first picture show our structure for the **backend**, the second picture shows the **frontend**-structure.
![B-MVC]
![F-MVC]

### 5.2 Architecturally Significant Design Packages

In this section you can find the generated class diagrams for backend and frontend. We used the same color coding as in section 2 to highlight the MVC pattern.

![B-CD]
backend class diagram

![F-CD]
frontend class diagram

## 6. Process View

n/a

## 7. Deployment View

![DEPVIEW]

## 8. Implementation View

n/a

## 9. Data View
This is a representation of our database schema including relationships between the entities. The current implemented state only involves the entities "Section" and "Card".

![DBS]


## 10. Size and Performance

n/a

## 11. Quality

We used different aspects to make sure that we get the best possible code quality possible. This makes it easy to extend and maintain the application. 
Our measures includ using Metrics and Codacy to have a constant overview over the current measured quality of the code. Therefore we were able to get a total grade of A for the frontend and B for the Backend. 
Also, apart from using continuous integration, we (re-) used software patterns to make the code as selfexplanatory as possible. These are further explained in chapter 12.

![cq] 

## 12. Pattern
Here you can see the patterns we used while coding. We used Data Transfer Objects which are objects with the single purpose of transmitting data. This allows to introduce an extra layer e.g. between services and controller.
We have two different DTOs – sections and cards. Both use the same structure as can be seen for the section in the first picture.
The other pictures show the implementation of this pattern in backend and frontend.
![OUMLBPCU]

![OUMLBP]

![OUMLFPC]
![OUMLFPS]

<!-- Picture-Link definitions: -->

[oucd]: ./UseCases/overall-use-case-diagram.png "Overall Use Case Diagramm"
[dbs]: ./ArchitectureDiagrams/database_scheme.png "Database Scheme"
[mvc]: ./ArchitectureDiagrams/MVC.png "General MVC Scheme"
[cdmvcb]: ./ArchitectureDiagrams/class-diagram-mvc.png "MVC for Backend"
[cdmvcf]: ./ArchitectureDiagrams/class-diagram-mvc-fe.png "MVC for Frontend"
[depview]: ./ArchitectureDiagrams/DepView.png "Deployment View"
[oumlbp]: ./ArchitectureDiagrams/backend/pattern.png "Overall UML Backend with Pattern"
[oumlbpcu]: ./ArchitectureDiagrams/backend/class-diagram-closeup.png "Overall UML Backend with Pattern (close up)"
[oumlfpc]: ./ArchitectureDiagrams/frontend/pattern-card.png "Overall UML Frontend with pattern (Card)"
[oumlfps]: ./ArchitectureDiagrams/frontend/pattern-section.png "Overall UML Frontend with pattern (Section)"
[b-mvc]: ./ArchitectureDiagrams/backend/mvc-architecture.png "Backend MVC Architecture"
[f-mvc]: ./ArchitectureDiagrams/frontend/mvc-architecture.png "Frontend MVC Architecture"
[b-cd]: ./ArchitectureDiagrams/backend/class-diagram.png "Backend Class Diagram"
[f-cd]: ./ArchitectureDiagrams/frontend/class-diagram.png "Frontend Class Diagram"
[cq]: ./ArchitectureDiagrams/Quality.png "Code Quality"
