# Use-Case Specification: Show Favourite

## 1. General

Sections and Cards can be marked as favourite. These favourites are shown on the dashboard as well as in the sections where they were created. This allows quick navigation from the dashboard for the user.

### 1.1 Brief Description

Sections and Cards can be marked as favourite. This is displayed in the header (for sections only) or through the 'heart' symbol when displayed in a list. Favourite items in the list have a filled 'heart' symbol, normal items are unfilled. 

All favourite items are shown on the dashboard.

### 1.2 Mock-up

![Dashboard](./mock-ups/dashboard.png)

## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
![Flowchart](./flow-chart.png)

## 3. Special Requirements

n/a

## 4. Preconditions

n/a

## 5. Postconditions

n/a

## 6. Extension Points

n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)
