# Use-Case Specification: Order Section In Hierarchy

## 1. General

Sections are ordered in a hierachy with a parent. This should be visible and in an intuitive order for the user.

### 1.1 Brief Description

Sections are structured in a hierarchy. The parent is determined upon creating a child. A user can navigate through the sections via a breadcrumb. The root section is displayed through a 'home' symbol.

### 1.2 Mock-up

![Mockup](./mock-up.png)

## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
![Flowchart](./flow-chart.png)

## 3. Special Requirements

n/a

## 4. Preconditions

There has to be a parent with a linked child section.

## 5. Postconditions

n/a


## 6. Extension Points

n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)
