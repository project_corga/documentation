# Use-Case Specification: Mark as Favourite

## 1. General

Sections and Cards can be marked as favourite. This allows quick navigation from the dashboard for the user.

### 1.1 Brief Description

Sections and Cards can be marked as favourite. This is possible in the 'edit'-form, the header (for sections only) or through the 'heart' symbol when displayed in a list.

### 1.2 Mock-up

![ListAndHeader](./mock-ups/list-and-header.png)
![EditForm](./mock-ups/edit-form.png)


## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
![Flowchart](./flow-chart.png)

## 3. Special Requirements

n/a

## 4. Preconditions

The to be marked Card or Section exist

## 5. Postconditions

The property "favourite" changed


## 6. Extension Points

n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)

