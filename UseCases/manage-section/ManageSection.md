# Use-Case Specification: Manage Section

## 1. General
A short overview of the use case including first mock-ups.

### 1.1 Brief Description

A user can manage sections. Sections exist to structure data (comparable to a folder in your locale file-system). The "manage" behaviour is defined by the CRUD standard. 

### 1.2 Mock-up
#### 1.2.1 Create Section Mockup
![Create Mockup Large](./mock-ups/create-section.png)
#### 1.2.2 Delete Section Mockup
![Delete Mockup Large](./mock-ups/delete-section.png)
#### 1.2.3 Update Section Mockup
![Update Mockup Large](./mock-ups/update-section.png)
#### 1.2.4 View Section Mockup
![View Mockup Large](./mock-ups/view-section.png)


## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
Here you can see the basic flowchart for this CRUD. 

![Flowchart](./flow-chart.png)

### 2.2 Feature Files

We decided not to use cucumber for integration/system testing and instead use Cypress. However we already created some feature-files that are not working but still exist. They can be found here:
* [create-section feature-file](/cucumber-tests/src/test/resources/CRUD_Section/create.feature)
* [delete-section feature-file](./cucumber-tests/src/test/resources/CRUD_Section/delete.feature)

## 3. Special Requirements
There always has to be a root-section (it's special because it has no parent). This section cannot be deleted!

## 4. Preconditions
A short overview of what is required to conduct this use case.
### 4.1 Create Section 
A parent section has to exist.

## 5. Postconditions

Description of what is required after the user submitted the form.
### 5.1 Create Section
The parent-sections attribute "children" has to be updated as well.
### 5.2 Delete Section
After deleting a section, its children-sections get deleted as well (CASCADING delete).

## 6. Extension Points
n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)
