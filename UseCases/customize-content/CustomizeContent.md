# Use-Case Specification: Customize Content

## 1. General

This is one of the major functionalities. We want to make it possible to display all kinds of data on the cards. These could include lists, documents, graphics, appointments, tables, code snippets, formulas and so on.

### 1.1 Brief Description

Cards contain content. This content should be as flexible in displaying as possible. Therefore HTML-tags should be supported while visualizing. These tags have to be carefully evaluated to prevent security threats (e.g. any script-tags are not allowed).

### 1.2 Mock-up

An example of how data could be visualized in a table form.

![Mockup](./mock-up.png)

## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
![Flowchart](./flow-chart.png)

## 3. Special Requirements

n/a

## 4. Preconditions

n/a

## 5. Postconditions

n/a


## 6. Extension Points

n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)

