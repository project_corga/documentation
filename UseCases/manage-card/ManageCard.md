# Use-Case Specification: Manage Card

## 1. General
A short overview of the Use-Case including first mock-ups.

### 1.1 Brief Description

A user can manage cards. Cards exist to store information. They are always linked to a parent section. The "manage" behaviour is defined by the CRUD standard. 

### 1.2 Mock-up
#### 1.2.1 Create Card Mockup
![Create Mockup Large](./mock-ups/create-card.png)
#### 1.2.2 Delete Card Mockup
![Delete Mockup Large](./mock-ups/delete-card.png)
#### 1.2.3 Update Card Mockup
![Update Mockup Large](./mock-ups/update-card.png)
#### 1.2.4 View Card Mockup
![View Mockup Large](./mock-ups/view-card.png)


## 2. Flow of Events

A flowchart about what happens on each side of the application for this specific use case.

### 2.1 Basic Flow
Here you can see the basic flowchart for this CRUD. 

![Flowchart](./flow-chart.png)

## 3. Special Requirements
If a parent section gets deleted all dependend cards get deleted as well (CASCADING delete).

## 4. Preconditions
A short overview of what is required to conduct this use case.
### 4.1 Create/Update Card: Parent Section
There has to be a parent section.

## 5. Postconditions
Description of what is required after the user submitted the form.

### 5.1 Create Card 
Card is correctly linked with its parent section and therefore it's listed in the "cards" attribute of its parent.

### 5.2 Delete Card
The parent-section "cards"-attribute gets updated as well.

## 6. Extension Points
n/a

## 7. Function Points
Here you can see how the Function Points for this UseCase were calculated.

| Abbrevation | Explanation                            |
| ----------- | -------------------------------------- |
| DET         | Data Element Type                      |
| RET         | Record Element Type                    |
| FTR         | File Type Reference                    |


![Function Points](./function-points-table.png)

The **Complexity Adjustment Table** that is needed for the calculation was created with Tiny Tools Function Point Generator

![OCT](../Complexity_Adjustment_Table.png)
