# Test plan

- [Test plan](#test-plan)
  - [1. Introduction](#1-introduction)
    - [1.1 Purpose](#11-purpose)
    - [1.2 Scope](#12-scope)
    - [1.3 Intended Audience](#13-intended-audience)
    - [1.4 Document Terminology and Acronyms](#14-document-terminology-and-acronyms)
    - [1.5 References](#15--references)
  - [2. Evaluation Mission and Test Motivation](#2-evaluation-mission-and-test-motivation)
    - [2.1 Background](#21-background)
    - [2.2 Evaluation Mission](#22-evaluation-mission)
    - [2.3 Test Motivators](#23-test-motivators)
  - [3. Target Test Items](#3-target-test-items)
  - [4. Outline of Planned Tests](#4-outline-of-planned-tests)
    - [4.1 Outline of Test Inclusions](#41-outline-of-test-inclusions)
    - [4.2 Outline of Other Candidates for Potential Inclusion](#42-outline-of-other-candidates-for-potential-inclusion)
    - [4.3 Outline of Test Exclusions](#43-outline-of-test-exclusions)
  - [5. Test Approach](#5-test-approach)
    - [5.1 Testing Techniques and Types](#51-testing-techniques-and-types)
      - [5.1.1 Unit Testing](#511-unit-testing)
      - [5.1.2 User Interface Testing](#512-user-interface-testing-cypress)
      - [5.1.3 Integration Testing ](#513-integration-testing)
      - [5.1.4 Performance Profiling ](#514-performance-profiling)
      - [5.1.5 Load Testing ](#515-load-testing)
      - [5.1.6 Stress Testing ](#516-stress-testing)
      - [5.1.7 Volume Testing ](#517-volume-testing)
      - [5.1.8 Security and Access Control Testing](#518-security-and-access-control-testing)
      - [5.1.9 Failover and Recovery Testing ](#519-failover-and-recovery-testing)
      - [5.1.10 Configuration Testing ](#5110-configuration-testing)
      - [5.1.11 Installation Testing ](#5111-installation-testing)
  - [6. Entry and Exit Criteria](#6-entry-and-exit-criteria)
    - [6.1 Test Plan](#61-test-plan)
      - [6.1.1 Test Plan Entry Criteria](#611-test-plan-entry-criteria)
      - [6.1.2 Test Plan Exit Criteria](#612-test-plan-exit-criteria)
  - [7. Deliverables](#7-deliverables)
    - [7.1 Test Evaluation Summaries](#71-test-evaluation-summaries)
    - [7.2 Reporting on Test Coverage](#72-reporting-on-test-coverage)
    - [7.3 Perceived Quality Reports](#73-perceived-quality-reports)
    - [7.4 Incident Logs and Change Requests](#74-incident-logs-and-change-requests)
    - [7.5 Smoke Test Suite and Supporting Test Scripts](#75-smoke-test-suite-and-supporting-test-scripts)
  - [8. Testing Workflow](#8-testing-workflow)
  - [9. Environmental Needs](#9-environmental-needs)
    - [9.1 Base System Hardware](#91-base-system-hardware)
    - [9.2 Base Software Elements in the Test Environment](#92-base-software-elements-in-the-test-environment)
    - [9.3 Productivity and Support Tools](#93-productivity-and-support-tools)
  - [10. Responsibilities, Staffing, and Training Needs](#10-responsibilities-staffing-and-training-needs)
    - [10.1 People and Roles](#101-people-and-roles)
    - [10.2 Staffing and Training Needs](#102-staffing-and-training-needs)
  - [11. Iteration Milestones](#11-iteration-milestones)
  - [12. Risks, Dependencies, Assumptions, and Constraints](#12-risks-dependencies-assumptions-and-constraints)
  - [13. Management Process and Procedures](#13-management-process-and-procedures)

## 1. Introduction

### 1.1 Purpose

The purpose of the Iteration Test Plan is to gather all of the information necessary to plan and control the test effort for a given iteration.
It describes the approach to testing the software. This Test Plan for CORGA supports the following objectives:

- Identifies the items that should be targeted by the tests.
- Identifies the motivation for and ideas behind the test areas to be covered.
- Outlines the testing approach that will be used.
- Identifies the required resources and provides an estimate of the test efforts.

### 1.2 Scope

This document describes the following different types of testing that are used in the project:

- Unit tests
- Integration tests
- System tests
- Installation tests

### 1.3 Intended Audience

This test plan is mainly for internal testing purposes.

### 1.4 Document Terminology and Acronyms

| Abbr | Abbreviation                        |
| ---- | ----------------------------------- |
| n/a  | not applicable                      |
| SAD  | Software Architecture Document      |
| SRS  | Software Requirements Specification |

### 1.5 References

| Title                                                 |   Date    | Publishing organization |
| ----------------------------------------------------- | :-------: | ----------------------- |
| [Blog](https://corga898830573.wordpress.com/)         | Apr. 2021 | CORGA                   |
| [GitLab Repository](https://gitlab.com/project_corga) | Apr. 2021 | CORGA                   |
| [SRS](./SRS.md)                                       | Apr. 2021 | CORGA                   |
| [SAD](./SAD.md)                                       | Apr. 2021 | CORGA                   |

## 2. Evaluation Mission and Test Motivation

### 2.1 Background

The tests will mainly be written to ensure that any changes in the future don't break the application. Developers should run tests regularely and take a look at the test coverage to make sure to prevent future bugs and mistakes as early as possible.

### 2.2 Evaluation Mission

- find problems
- find existing bugs (if there are any)
- give a hint about the product quality

### 2.3 Test Motivators

With the help of our tests we are able to maintain a good code quality and ensure the functionality of our product.

## 3. Target Test Items

The listing below identifies those test items software, hardware, and supporting product elements that have been identified as targets for testing. As we don't have the infrastructure to perform hardware and operating system tests, our tests will focus on the code we write.

- Angular Frontend
- Spring backend

## 4. Outline of Planned Tests

### 4.1 Outline of Test Inclusions

_Angular Frontend_:

- Unit testing
- System tests

_Spring Backend_:

- Integration testing

The test code is not tested and therefore does not count into the test coverage.

### 4.2 Outline of Other Candidates for Potential Inclusion

n/a

### 4.3 Outline of Test Exclusions

Hardware and operating systems will not be tested as we don't have the infrastructure to do this. Stress tests, load/performance and security tests are also not part of this project.

## 5. Test Approach

### 5.1 Testing Techniques and Types

#### 5.1.1 Unit Testing

Unit tests are used to tests some parts of the code independently while mocking the rest of the application. This technique is used to ensure the functionality of all components in our frontend.

|                        | Description                                                            |
| ---------------------- | ---------------------------------------------------------------------- |
| Technique Objective    | Ensure that the implemented code works as expected                     |
| Technique              | Implement test methods using Jasmine and Karma.js (Frontend)           |
| Oracles                | Test execution logs results to the command line and Code coverage tool |
| Required Tools         | Karma, Jasmine and Chrome                                              |
| Success Criteria       | All tests pass and tests coverage is 80% or higher                     |
| Special Considerations | -                                                                      |

#### 5.1.2 User Interface Testing (Cypress)

|                        | Description                                            |
| ---------------------- | ------------------------------------------------------ |
| Technique Objective    | Every possible UI scenario should work flawless        |
| Technique              | Cypress testing                                        |
| Oracles                | The site looks as expected after performing a scenario |
| Required Tools         | Cypress                                                |
| Success Criteria       | All tests passed                                       |
| Special Considerations | -                                                      |

#### 5.1.3 Integration Testing

|                        | Description                                                                         |
| ---------------------- | ----------------------------------------------------------------------------------- |
| Technique Objective    | Ensure that the implemented code works as expected                                  |
| Technique              | Integration testing                                                                 |
| Oracles                | Test execution logs results to the command line, in Pipeline and Code coverage tool |
| Required Tools         | JUnit and IntelliJ                                                                  |
| Success Criteria       | All tests passed and code coverage is 80% or higher                                 |
| Special Considerations | -                                                                                   |

#### 5.1.4 Performance Profiling

n/a

#### 5.1.5 Load Testing

n/a

#### 5.1.6 Stress Testing

n/a

#### 5.1.7 Volume Testing

n/a

#### 5.1.8 Security and Access Control Testing

n/a

#### 5.1.9 Failover and Recovery Testing

n/a

#### 5.1.10 Configuration Testing

n/a

#### 5.1.11 Installation Testing

|                        | Description                                                                                                         |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------- |
| Technique Objective    | Installation should be intuitive, quick and flawless                                                                |
| Technique              | Installation by outsiders                                                                                           |
| Oracles                | All required tools are installed                                                                                    |
| Required Tools         | IntelliJ, Angular, NPM                                                                                              |
| Success Criteria       | The application starts and works as expected after the installation, The installation takes about 5-10 minutes max. |
| Special Considerations | -                                                                                                                   |

## 6. Entry and Exit Criteria

n/a

### 6.1 Test Plan

#### 6.1.1 Test Plan Entry Criteria

n/a

#### 6.1.2 Test Plan Exit Criteria

All tests pass successfully.

## 7. Deliverables

## 7.1 Test Evaluation Summaries

The tests in our backend produce evaluation summaries. Each time a commit is pushed the CI of GitLab is used to run all tests automatically.

## 7.2 Reporting on Test Coverage

For our test coverage in the frontend we used the [karma coverage reporter](https://www.npmjs.com/package/karma-coverage).

## 7.3 Perceived Quality Reports

The code quality tool for the frontend is ES Lint.

## 7.4 Incident Logs and Change Requests

Incident Logs and Change Requests are handled via Git and our project management tool YouTrack.

## 7.5 Smoke Test Suite and Supporting Test Scripts

n/a

## 8. Testing Workflow

Our testing process is a part of our development process and consists of testing in our IDE after implementing a new feature. With every commit in our backend our integration tests run automatically.

## 9. Environmental Needs

### 9.1 Base System Hardware

n/a

### 9.2 Base Software Elements in the Test Environment

The following base software elements are required in the test environment for this Test Plan.

| Software Element Name | Type and Other Notes       |
| --------------------- | -------------------------- |
| IntelliJ              | Test Runner / IDE          |
| Visual Studio Code    | IDE                        |
| Jasmine               | Unit testing library       |
| Karma                 | Test Runner                |
| JUnit                 | Testing library            |
| Cypress               | End-to-end testing library |

### 9.3 Productivity and Support Tools

The following tools will be employed to support the test process for this Test Plan.

| Tool Category or Type             | Tool Brand Name | Vendor or In-house | Version |
| --------------------------------- | --------------- | ------------------ | ------- |
| Test Management                   | Karma.js        | Open-Source        | 6.3.2   |
| Test Coverage Monitor or Profiler | Karma coverage  | Open-Source        | 2.0.3   |
| Project Management                | YouTrack        | JetBrains          | N/A     |
| DBMS tools                        | IntelliJ        | JetBrains          | 2021.1  |

## 10. Responsibilities, Staffing, and Training Needs

### 10.1 People and Roles

This table shows the staffing assumptions for the test effort.
|Role|Minimum Resources Recommended(number of full-time roles allocated)|Specific Responsibilities or Comments|
|---|---|---|
|Test Manager|1|Provides management oversight. Responsibilities include: planning and logistics agree mission identify motivators acquire appropriate resources present management reporting advocate the interests of test evaluate effectiveness of test effort|
|Test Designer|1|Defines the technical approach to the implementation of the test effort. Responsibilities include: define test approach define test automation architecture verify test techniques define testability elements structure test implementation|
|Tester|1|Implements and executes the tests. Responsibilities include: implement tests and test suites execute test suites log results analyze and recover from test failures document incidents|
|Test System Administrator|1|Ensures test environment and assets are managed and maintained. Responsibilities include: administer test management system install and support access to, and recovery of, test environment configurations and test labs|
|Implementer|1|Implements and unit tests the test classes and test packages. Responsibilities include: creates the test components required to support testability requirements as defined by the designer|

### 10.2 Staffing and Training Needs

n/a

## 11. Iteration Milestones

| Milestone          | Planned Start Date | Actual Start Date | Planned End Date | Actual End Date |
| ------------------ | ------------------ | ----------------- | ---------------- | --------------- |
| Have Unit Tests    | 01.11.2020         | 25.11.2020        | 01.06.2021       | 01.06.2021      |
| Have Cypress Tests | 01.05.2021         | 20.5.2021         | 15.06.2021       | 02.06.2021      |
| 80% coverage       | 01.11.2020         | 25.11.2020        | 01.06.2021       | 01.06.2021      |

## 12. Risks, Dependencies, Assumptions, and Constraints

| Risk                            | Mitigation Strategy   | Contingency (Risk is realized) |
| ------------------------------- | --------------------- | ------------------------------ |
| Testing scenario is not covered | Aim for high coverage | Add scenario                   |

## 13. Management Process and Procedures

n/a
